function isDummyString(item) {
    if (typeof(item) === 'string') {
        return item.toUpperCase().indexOf('DUMMY') === 0;
    }
    else {
        return false;
    }
}


/**
 * Remove any "DUMMY" elements at the end of arrays or "DUMMY" object keys
 * from JSON returned by the Account4 server.
 * Note:
 * - Callers of this function should only pass one argument: the JSON object
 */
function removeDummies(json, objectsSoFar) {
    if (typeof(json) !== 'object') {
        return;
    }
    if (!objectsSoFar) {
        objectsSoFar = [];
    }
    objectsSoFar.push(json);

    if (json.constructor === Array && json.length) {
        // Remove last item of array if it is a DUMMY item:
        var lastElem = json[json.length - 1];
        if (isDummyString(lastElem)) {
            json.splice(-1, 1);
        }
    }
    else {
        for (var attrib in json) {
            if (!json.hasOwnProperty(attrib)) {
                continue;
            }
            if (isDummyString(attrib)) {
                delete json[attrib];
                continue;
            }
            var value = json[attrib];
            if (value && typeof(value) === 'object' && objectsSoFar.indexOf(value) === -1) {
                removeDummies(value, objectsSoFar);
            }
        }
    }
}


module.exports = {
    removeDummies: removeDummies
};

