/*jslint node: true*/

"use strict";


const url = require('url');
const request = require('request');
const jsonUtils = require('./json-utils.js');
const fs = require('fs');
const DEBUG = false;
const MILLIS_IN_ONE_HOUR = 60 * 60 * 1000;


let config = {};
fs.exists('./config.js', function (exists) {
    if (exists) {
        config = require('./config.js');
    }
    if (typeof(config) === 'undefined') {
        config = {};
    }
});


function pathToDLL(theRequest) {
    if (config.fixedPathToAccount4DLL) {
        return config.fixedPathToAccount4DLL;
    }

    // It is assumed the Account4 DLL URL has the following structure:
    //     http(s)://HOST/INSTANCE/Server/INSTANCE.dll
    // and requests to this a4-rest-service would start with:
    //     http(s)://HOST/INSTANCE/api
    // and the incoming path would start with:
    //     HOST/INSTANCE/api
    // and the second path component (INSTANCE) represents the Account4 instance
    let pathComponents = theRequest.path.split('/');
    let a4ServerInstance = pathComponents[1];
    let dllName = a4ServerInstance + '.dll';
    let protocol = theRequest.secure ? 'https' : 'http';
    return protocol + '://' +
        theRequest.headers.host + '/' +
        a4ServerInstance +
        '/Server/' +
        dllName;
}


function mergeObjects(obj1, obj2) {
    let merged = {};
    let attrName;
    for (attrName in obj1) { merged[attrName] = obj1[attrName]; }
    for (attrName in obj2) { merged[attrName] = obj2[attrName]; }
    return merged;
}


function determineAPIName(resource) {
    return 'A4APIBASE' + resource;
}


function determineCRUDAction(theRequest) {
    let httpMethod = theRequest.method;
    if (httpMethod === 'GET') {
        return 'READ';
    }
    else if (httpMethod === 'POST') {
        return 'CREATE';
    }
    else if (httpMethod === 'PUT') {
        return 'UPDATE';
    }
    else if (httpMethod === 'DELETE') {
        return 'DELETE';
    }
    else {
        return httpMethod;
    }
}


function parsePathResource(theRequest) {
    let dllURL = pathToDLL(theRequest);
    let dllPath = dllURL.split('//')[1];
    let dllPathParts = dllPath.split('/');
    let requestPathParts = theRequest.path.split('/');

    // Sample dllPath:
    //     MyServer/A4Instance/Server/A4Instance.dll
    // Sample API request path from `theRequest.path` would start with:
    //     MyServer/A4Instance/api
    // and the resource name would be in the same path level as the DLL:
    //     MyServer/A4Instance/api/MyResource
    let resourcePosition = dllPathParts.length - 1;

    let resource = requestPathParts[resourcePosition];
    let resourceId = null;
    if (requestPathParts.length >= resourcePosition) {
        resourceId = requestPathParts[resourcePosition + 1];
    }
    return {
        resource: resource,
        resourceId: resourceId
    };
}


function applyDefaultParams(params, theRequest) {
    let resourceParts = parsePathResource(theRequest);
    if (resourceParts.resourceId) {
        params['K' + resourceParts.resource] = resourceParts.resourceId;
    }
    params.FORMID = determineAPIName(resourceParts.resource);
    params.KCRUD = determineCRUDAction(theRequest);
    if (params.KCRUD === 'CREATE' && !params.KTEMPID) {
        params.KTEMPID = guid();
    }
    if (!params.KTEMPID) {
        params.KTEMPID = ''; // Reset. Make sure server doesn't reuse old value
    }
    if (resourceParts.resource === 'LOGIN') {
        if (!params.FTYPE) {
            params.FTYPE = 'L';
        }
    }
    else {
        if (!params.INIT) {
            params.INIT = 'Y';
        }
        if (!params.FTYPE) {
            params.FTYPE = 'R';
        }
        if (!params.TSESSIONSAVE) {
            params.TSESSIONSAVE = 'NO';
        }
        if (!params.INKEY) {
            params.INKEY = theRequest.cookies.inkey;
        }
    }
}


// Covert the given param name into an Account4 compatible "K" parameter
// in all-uppercase
// except if the param is one of the special Account4 parameters, like INKEY
function makeA4KParamIfNecessary(param) {
    if (param === 'FTYPE'   || param === 'INIT' ||
        param === 'USERID'  || param === 'UPSW' ||
        param === 'INKEY'   || param === 'TSESSIONSAVE' ||
        param === 'FORMID') {
        return param;
    }
    param = (param || '').toUpperCase();
    if (param && param[0] === 'K') {
        return param; // Do not add 'K' prefix if param already starts with K
    }
    else {
        return 'K' + param.toUpperCase();
    }
}


function makeServerCallURL(dllURL, params) {
    let paramPairs = [];
    for (let param in params) {
        if (param === 'host') {
            continue;
        }
        var a4Param = makeA4KParamIfNecessary(param);
        paramPairs.push(a4Param + '=' + params[param]);
    }
    return dllURL + '?' + paramPairs.join('&');
}


function isLoginAPI(url) {
    return url.indexOf('A4APIBASELOGIN') > -1;
}


function currentTimeMillis() {
    return +new Date();
}


// If an auto-login is configured for current request's Account4 Server,
// then use it!
function handleAutoLoginIfConfigured(params, dllURL, a4ServerInstance) {
    let a4ServerConfig = config[a4ServerInstance.toLowerCase()];
    if (!a4ServerConfig) {
        return;
    }
    let autoLoginConfig = a4ServerConfig.autologin;
    if (autoLoginConfig) {
        let user = autoLoginConfig.user;
        let password = autoLoginConfig.password;
        if (user && password) {
            if (hasValidCachedInkey(autoLoginConfig)) {
                params.FTYPE = 'R';
                params.INKEY = autoLoginConfig.cachedInkey;
                delete params.USERID;
                delete params.UPSW;
                return;
            }

            params.USERID = user;
            params.UPSW = password;
            params.FTYPE='L';
            delete params.INKEY;

            // Make a second LOGIN call to the Account4 server to get an INKEY.
            // With this, subsequent calls can reuse the INKEY until it expires.
            // Sample login URL:
            // http://natasha/A4RestDemoSQL/Server/A4RestDemoSQL.dll?FORMID=A4APIBASELOGIN&FTYPE=L&USERID=ADMIN&UPSW=ADMIN
            let loginCallURL = dllURL +
                '?FORMID=A4APIBASELOGIN&FTYPE=L&USERID=' +
                user + '&UPSW=' + password;
            request(loginCallURL, function(error, response, body) {
                let json = JSON.parse(body);
                let inkey = json.USER.PSAINKEY;
                autoLoginConfig.cachedInkey = inkey;
                autoLoginConfig.cachedInkeyTime = currentTimeMillis();
            });
        }
    }
}


function hasValidCachedInkey(autoLoginConfig) {
    if (autoLoginConfig.cachedInkey && autoLoginConfig.cachedInkeyTime) {
        let cachedInkeyAgeMillis = currentTimeMillis() - autoLoginConfig.cachedInkeyTime;
        let maxCacheAgeMillis = autoLoginConfig.cachedSessionTimeoutMillis || MILLIS_IN_ONE_HOUR;
        return cachedInkeyAgeMillis < maxCacheAgeMillis;
    }
    return false;
}


function a4ServerInstanceFromDll(dllURL) {
    let pathComponents = dllURL.split('/');
    let dll = pathComponents[pathComponents.length - 1];
    return dll.split('.')[0];
}


function relayToAccount4Server(theRequest, theResponse) {
    let dllURL = pathToDLL(theRequest);
    let urlParts = url.parse(theRequest.url, true);
    let params = mergeObjects(theRequest.body || {}, urlParts.query);
    applyDefaultParams(params, theRequest);
    let a4ServerInstance = a4ServerInstanceFromDll(dllURL);
    handleAutoLoginIfConfigured(params, dllURL, a4ServerInstance, theResponse);

    let serverCallURL = makeServerCallURL(dllURL, params);
    request(serverCallURL, function(error, response, body) {
        try {
            if (DEBUG) {
                theResponse.send(serverCallURL);
                return;
            }
            let json;
            try {
                json = JSON.parse(body);
                jsonUtils.removeDummies(json);
            }
            catch (exc) {
                return theResponse
                    .status(500)
                    .send(makeErrorJSON(exc, body, theRequest, serverCallURL));
            }
            if (isLoginAPI(serverCallURL)) {
                theResponse.cookie('inkey', json.USER.PSAINKEY);
            }
            if (theRequest.method === 'POST') {
                theResponse.status(201);
            }
            theResponse.send(json);
        }
        catch (err) {
            let message = 'error with Account4 server call. ' + err + '\n\n' + serverCallURL + '\n\n' + body;
            theResponse.send(makeErrorJSON(err, message, theRequest, serverCallURL));
        }
    });
}


function debug(theRequest, theResponse) {
    theResponse.send('debug');
}


function guid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        let r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
}


function makeErrorJSON(exc, text, theRequest, a4Url) {
    let extractedError = extractErrorMessage(exc, text);
    return {
        error: {
            requestPath: theRequest.path,
            requestMethod: theRequest.method,
            a4url: a4Url,
            message: extractedError
        }
    };
}


function extractErrorMessage(error, data, defaultMessage) {
    let flattenedData = data.split('\n').join('').split('\r').join('');
    let regexes = [
        '<ERRORMSG>(.*)<param .*<param .*<param ',
        '<SAERROR.*CDATA.(.*)...</SAERROR>',
        '<param name="errortext" value="(.*)"/>'
    ];
    let errorMessage = null;
    for (let idx=0; idx<regexes.length; idx++) {
        var regex = regexes[idx];
        var re = new RegExp(regex);
        var matches = re.exec(flattenedData);
        if (matches && matches.length >= 2) {
            errorMessage = matches[1];
            errorMessage = errorMessage.replace(/&quot;/g, '"');
            break;
        }
    }
    if (errorMessage) {
        if (flattenedData.indexOf('Your session has timed out') > -1)
            errorMessage = 'Your session has timed out.\n' + errorMessage;
        return errorMessage;
    }
    if (defaultMessage)
        return defaultMessage;
    if (error && error.name == 'SyntaxError') {
        return 'Syntax error occurred, while parsing data from server.\n' + error.message;
    }
    return data;
}



module.exports = {
    relayToAccount4Server: relayToAccount4Server,
    debug: debug
};
