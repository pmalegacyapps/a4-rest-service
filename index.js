/*jslint node: true*/
"use strict";

const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const a4ServerCall = require('./a4-server-call.js');
const app = express();


////////////////////////////////////////////////////////////////////////////////
// Setup Express Middleware:
////////////////////////////////////////////////////////////////////////////////
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.use(bodyParser.json());
app.use(cookieParser());


////////////////////////////////////////////////////////////////////////////////
// Handle incoming request for each of the following four HTTP methods:
////////////////////////////////////////////////////////////////////////////////
app.get('*',    a4ServerCall.relayToAccount4Server);
app.post('*',   a4ServerCall.relayToAccount4Server);
app.put('*',    a4ServerCall.relayToAccount4Server);
app.delete('*', a4ServerCall.relayToAccount4Server);


////////////////////////////////////////////////////////////////////////////////
// Start the Node Server and let it listen for incoming requests:
////////////////////////////////////////////////////////////////////////////////
if (process.env.PORT) {
    app.listen(process.env.PORT);
}
else {
    var port = 8000;
    app.listen(port);
    console.log('listining on ' + port);
}
