The a4-rest-service is a server-side network module implemented in NodeJS.


# INSTALL NODE.JS

Install Node.js if it's not installed already.

from https://nodejs.org click the "Install" link to download the installer.
Select the 32-bit or 64-bit version depending on your system
If there are two versions to choose from, choose the "Mature and dependable" or
LTS version instead of the "Latest" version keep all the default options


# INSTALL THE IISNODE MODULE

It's managed by IIS using the iisnode module. Here is the iisnode project's
github page: https://github.com/tjanczuk/iisnode From that page, look for the
x86 (32-bit) or x64 (64-bit) link to download the installer file.
Run the installer to install iisnode.

**IMPORTANT**: Check and install all the iisnode prerequisites listed!!


# CLONE THE A4-REST-SERVICE REPOSITORY

On the server that hosts the Account4 instance, clone the a4-rest-service git
repository. Alternatively, request a ZIP file for the a4-rest-service with the
node_modules directory pre-packaged, and extract it to the server.

Clone the BitBucket a4-rest-service repository.

```
git clone https://bitbucket.org/pmalegacyapps/a4-rest-service
```

Next, "cd" into the cloned a4-rest-service directory and install the node
modules on which the a4-rest-service depends, by issuing the following command
in the command line:

```
npm install
```

Open the IIS configuration file "applicationHost.config which is located in the
C:\Windows\System32\inetsrv\config directory. Note: make sure to edit this file
using a 64-bit text editor if you're on 64-windows. Editing this file in a
32-bit editor will cause confusion because the editor will save it to a
different location!

Inside the applicationHost.config file, find the VirtualDirectory settings for
the Account4 instance that needs to use the a4-rest-service. Below is a sample
of the settings for the A4FrazerSQL instance:

```xml
<application path="/A4FrazerSQL" applicationPool="A4FrazerSQL">
    <virtualDirectory path="/" physicalPath="c:\psainstalls\A4FrazerSQL" />
    <virtualDirectory path="/Root" physicalPath="c:\psainstalls\A4FrazerSQL\root" />
    <virtualDirectory path="/Portal" physicalPath="c:\psainstalls\A4FrazerSQL\Portal" />
    <virtualDirectory path="/Help" physicalPath="c:\psainstalls\A4FrazerSQL\help" />
    <virtualDirectory path="/Server" physicalPath="c:\psainstalls\A4FrazerSQL\Server" />
    <virtualDirectory path="/Reports" physicalPath="c:\psainstalls\A4FrazerSQL\Reports" />
    <virtualDirectory path="/api" physicalPath="C:\project\a4-rest-service" />
</application>
```

Note: The last virtualDirectory line got added to handle the URLs that contain
the "/api" path following the A4FrazerSQL part of the URL.

Next, restart IIS and the integration should be complete


# OPTIONAL CONFIGURATION FOR A4-REST-SERVICE

There are further configuration options available for the a4-rest-service. To
enable the configuration options, make a copy of the config.js.sample file in
the a4-rest-server directory, and name it config.js

There are two configuration options:

## fixedPathToAccount4DLL

The the value of this property to the full http/https URL path to the Account4
instance's DLL, for example http://DEVSERVER/MyInstance/Server/MyInstance.dll
This can be useful if the URL structure does not follow the usual convension of
having the Account4 instance name as a separate path component.

Note: if this setting is present, the a4-rest-service can only work with one
Account4 instance, even if there are multiple Account4 instances installed on
the same server.

## autologin

This section can be set per Account4 instance, to let the a4-rest-service
automatically login with a specified user account. This way, the API caller
does not need to first call the LOGIN API to establish a session.

The top-level key is the name of the Account4 instance (use lowercase). The
second-level key is "autologin", and groups the three lowest-level keys:

* user: this is the Account4 account user to be used for the auto-login
* password: the Account4 account password to be used for the auto-login
* cachedSessionTimeoutMillis: Previous sessions shall be reused for subsequent
  requests. After this given timeout (in milliseconds), the a4-rest-service
  will create a fresh session


# Some REST concepts

With RESTful APIs, the URL is very important and follows certain convensions. The URL contains the name of the entity that one deals with. This entity is called the *Resource*. In general, the URL will look very clean and not cluttered with unnecessary parameters. Any fields that need to be transferred to the APIs are put into the request body, also referred to as the request "payload". The fields in the body (payload) will be a formatted JSON string.

With RESTful APIs, the HTTP methods (verbs) are used to express the different CRUD actions (Create, Read, Update and Delete).

Here are the four HTTP verbs that are used, and their meaning:

* HTTP GET:
    * with no ID in the URL: means get all the resources
    * with an ID in the URL: means get a specific resource
* HTTP POST:  means to create a new resource
* HTTP PUT: means to update a specific resource (ID specified in the URL)
* HTTP DELETE:  means to delete a specific resource (ID specified in the URL)

Here are a few sample URLs:

* GET http://natasha/A4RESTDemoSQL/api/EXPENSEREPORT (will get all expense reports)
* GET http://natasha/A4RESTDemoSQL/api/EXPENSEREPORT/123 (will get expense report with ID 123
* POST http://natasha/A4RESTDemoSQL/api/EXPENSEREPORT will create a new expense report
* PUT http://natasha/A4RESTDemoSQL/api/EXPENSEREPORT/123 will update expense report with ID 123
* DELETE http://natasha/A4RESTDemoSQL/api/EXPENSEREPORT/123 will delete expense report with ID 123

# How REST calls translate into A4 Forms

The Account4 DLL handles only handles two HTTP verbs: GET and POST.
The incoming REST API calls get translated into calls to Account4 DLL forms, as follows:

* FORMID is the name of the resource with the "A4APIBASE" prefix. For the above sample URLs, the FORMID would be A4APIBASEEXPENSEREPORT
* A K-parameter named KCRUD is supplied and it identifies the type of request: 
    * "READ" for HTTP GET method
    * "CREATE" for HTTP POST method
    * "UPDATE" for HTTP PUT method
    * "DELETE" for HTTP DELETE method

# How to make REST API calls

Below are a few samples on how to call the APIs in a RESTful manner. These snippets are taken from the demo.js file that can be found from the following repository: http://f11ev004:8000/a4restdemo/ (NOTE: that URL requires you to be on the VPN).

The following will do a POST call to create a new expense report. Note that the create call supplies many parameters in the request body (payload) under the "data" attribute in the object passed into the jQuery $.ajax method.

The $.ajax method takes two parameters:
* The URL to the resource URL. In the example below, it uses the apiURLFor function to determine the URL. It will be something like http://natasha/A4RESTDemoSQL/api/EXPENSEREPORT
* The second parameter is an object with name/value pairs for all the fields that the API expects.

The $.ajax method returns an object that represents the server call. On this object, one can call the "done" method. The example below shows that the done method gets called with a call-back function as parameter. This call-back function will be called as soon as the server's response comes back

```javascript
////////////////////////////////////////////////////////////////////////////
//     _____                            _                                 //
//    / ____|                          | |                                //
//   | |         _ __     _   _      __| |                                //
//   | |        | '__|   | | | |    / _` |                                //
//   | |____    | |      | |_| |   | (_| |                                //
//    \_____|   |_|       \__,_|    \__,_|            (Create new record) //
//                                                                        //
////////////////////////////////////////////////////////////////////////////
function createExpenseReport() {
    $.ajax(
        apiURLFor('EXPENSEREPORT'),
        {
            method: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({
                KNAME: "new report",
                KSTART: "01-Dec-2015",
                KFINISH: "31-Jan-2016",
                KRESID: 1,
                KDISPLAYID: 2,
                KREIMBURSEMENTCURRENCY: 1,
                KPAYMETHOD: 20,
                KTRANCURRENCY: 1,
                KACCTCAT: "",
                KACCTUNIT: 336,
                KACTIVITY: 1,
                KDEPARTMENT: "",
                KLOCATION: ""
            })
        }
    )
    .done(function(json) {
        console.log(json);
        // handle the JSON response further if needed ...
    });
}
```

The next example is a simple GET call to g et a list of all the expense reports:

```javascript
////////////////////////////////////////////////////////////////////////////
//              _____                    _                                //
//             |  __ \                  | |                               //
//      ___    | |__) |    _   _      __| |                               //
//     / __|   |  _  /    | | | |    / _` |                               //
//    | (__    | | \ \    | |_| |   | (_| |                               //
//     \___|   |_|  \_\    \__,_|    \__,_|            (read all records) //
//                                                                        //
////////////////////////////////////////////////////////////////////////////
function readAllExpenseReports() {
    $.ajax(
        apiURLFor('EXPENSEREPORT'),
        {
            method: 'GET',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }
    )
    .done(function(json) {
        var expenseReports = json.expenseReports;
        updateExpenseTable(expenseReports);
    });
}
```

The following example is another GET call, but to get the details of a specific expense report with the ID specified in the function parameter "expenseReportId"

```javascript
////////////////////////////////////////////////////////////////////////////
//              _____                    _                                //
//             |  __ \                  | |                               //
//      ___    | |__) |    _   _      __| |                               //
//     / __|   |  _  /    | | | |    / _` |                               //
//    | (__    | | \ \    | |_| |   | (_| |                               //
//     \___|   |_|  \_\    \__,_|    \__,_|      (read a specific record) //
//                                                                        //
////////////////////////////////////////////////////////////////////////////
function readExpenseReport(expenseReportId) {
    $.ajax(
        apiURLFor('EXPENSEREPORT', expenseReportId),
        {
            method: 'GET',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }
    )
    .done(function(json) {
        alert("Details of the expense report:\n\n" + JSON.stringify(json));
    });
}
```

The next example does a PUT, to update the name of the expense report:

```javascript
////////////////////////////////////////////////////////////////////////////
//                      _    _         _                                  //
//                     | |  | |       | |                                 //
//     ___     _ __    | |  | |     __| |                                 //
//    / __|   | '__|   | |  | |    / _` |                                 //
//   | (__    | |      | |__| |   | (_| |                                 //
//    \___|   |_|       \____/     \__,_|        (update specific record) //
//                                                                        //
////////////////////////////////////////////////////////////////////////////
function updateExpenseReport(expenseReport) {
    $.ajax(
        apiURLFor('EXPENSEREPORT', expenseReport.id),
        {
            method: 'PUT',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({
                KNAME: expenseReport.name
            })
        }
    );
}
```

The following does a DELETE to remove the expense report from the system:

```javascript
////////////////////////////////////////////////////////////////////////////
//                                _____                                   //
//                               |  __ \                                  //
//     ___     _ __     _   _    | |  | |                                 //
//    / __|   | '__|   | | | |   | |  | |                                 //
//   | (__    | |      | |_| |   | |__| |                                 //
//    \___|   |_|       \__,_|   |_____/         (delete specific record) //
//                                                                        //
////////////////////////////////////////////////////////////////////////////
function deleteExpenseReport(expenseReport) {
    $.ajax(
        apiURLFor('EXPENSEREPORT', expenseReport.id),
        {
            method: 'DELETE',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json'
        }
    );
}
```
