var PADDING = [
    '',
    '    ',
    '        ',
    '            '
];


function printObject(object, res, level) {
    level = level || 0;
    res.write('<ul>');
    for (var prop in object) {
        res.write('<li>' + prop + ': ');
        var value;
        if (prop === 'host') {
            value = '(could not access)';
        }
        else {
            value = object[prop];
        }
        if (typeof(value) === 'object') {
            if (level < 4) {
                printObject(value, res, level + 1);
            }
            else {
                res.write('[Object object]');
            }
        }
        else {
            res.write('value');
        }
        res.write('</li>');
    }
    res.write('</ul>');
}


function consoleLogObject(object, res, level) {
    level = level || 0;
    for (var prop in object) {
        var value;
        if (prop === 'host') {
            value = '(could not access)';
        }
        else {
            value = object[prop];
        }
        if (typeof(value) === 'function') {
            continue;
        }
        else if (typeof(value) === 'object') {
            console.log(PADDING[level] + prop + ': ');
            if (level < 1) {
                consoleLogObject(value, res, level + 1);
            }
            else {
                console.log(PADDING[level+1] + prop + ': [Object object]');
            }
        }
        else {
            console.log(PADDING[level] + prop + ': ' + value);
        }
    }
}



